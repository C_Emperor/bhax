import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import javax.imageio.ImageIO;

public class ASCIIFile {

public static void main(String[] args) throws IOException {

        //BufferedImage image = ImageIO.read(new File("/Users/mkyong/Desktop/logo.jpg"));
        BufferedImage image = null;
        Writer writer = null;
        try
		{
        		image = ImageIO.read(new File("img.png"));
        		writer = new FileWriter("out.txt");        		
       	}
        catch(IOException e)
        {}
        Graphics g = image.getGraphics();
        int height= image.getHeight();
        int width= image.getWidth();
        //        g.setFont(new Font("SansSerif", Font.BOLD, 24));

        Graphics2D graphics = (Graphics2D) g;
        graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        graphics.drawString("JAVA", 10, 20);

        //save this image
        //ImageIO.write(image, "png", new File("/users/mkyong/ascii-art.png"));

        
        for (int y = 0; y < height; y++) {
            StringBuilder sb = new StringBuilder();
            for (int x = 0; x < width; x++) {

                sb.append(image.getRGB(x, y) == -16777216 ? " " : "$");

            }

            if (sb.toString().trim().isEmpty()) {
                continue;
            }

            writer.write(sb.toString());
            writer.write(System.lineSeparator());
        }
        writer.close();

    }

}
