#include <iostream>
#include <string>

#include <boost/filesystem.hpp>

//$ g++ sj.cpp -o sj -lboost_filesystem

int count=0;

void read_acts ( boost::filesystem::path path)
{

        if ( is_regular_file ( path ) ) {

                std::string ext ( ".java" );
                if ( !ext.compare ( boost::filesystem::extension ( path ) ) ) 
                {

			count++;
                }

        } else if ( is_directory ( path ) )
                for ( boost::filesystem::directory_entry & entry : boost::filesystem::directory_iterator ( path ) )
                        read_acts ( entry.path());

}
int main ( int argc, char *argv[] )
{
	read_acts(argv[1]);
	std::cout<<count<<std::endl;
}
