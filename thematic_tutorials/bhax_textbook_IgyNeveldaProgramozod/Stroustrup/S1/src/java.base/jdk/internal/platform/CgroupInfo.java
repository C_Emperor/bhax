/*
 * Copyright (c) 2020, Red Hat Inc.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package jdk.internal.platform;

/**
 * Data structure to hold info from /proc/self/cgroup
 *
 * man 7 cgroups
 *
 * @see CgroupSubsystemFactory
 */
class CgroupInfo {

    private final String name;
    private final int hierarchyId;
    private final boolean enabled;

    private CgroupInfo(String name, int hierarchyId, boolean enabled) {
        this.name = name;
        this.hierarchyId = hierarchyId;
        this.enabled = enabled;
    }

    String getName() {
        return name;
    }

    int getHierarchyId() {
        return hierarchyId;
    }

    boolean isEnabled() {
        return enabled;
    }

    static CgroupInfo fromCgroupsLine(String line) {
        String[] tokens = line.split("\\s+");
        if (tokens.length != 4) {
            return null;
        }
        // discard 3'rd field, num_cgroups
        return new CgroupInfo(tokens[0] /* name */,
                              Integer.parseInt(tokens[1]) /* hierarchyId */,
                              (Integer.parseInt(tokens[3]) == 1) /* enabled */);
    }

}
