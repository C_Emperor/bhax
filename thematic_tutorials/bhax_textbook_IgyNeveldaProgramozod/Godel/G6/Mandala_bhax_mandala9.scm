;Lekérdezzük a lista x-edik elemét
(define (elem x lista)

    (if (= x 1) (car lista) (elem (- x 1) ( cdr lista ) ) )

)
;Szöveg magassáa font alapján
(define (text-width text font fontsize)
(let*
    (
        (text-width 1)
    )
    (set! text-width (car (gimp-text-get-extents-fontname text fontsize PIXELS font)))    

    text-width
    )
)
;Szöveg magassáa és szélessége
(define (text-wh text font fontsize)
(let*
    (
        (text-width 1)
        (text-height 1)
    )
    ;;;
    (set! text-width (car (gimp-text-get-extents-fontname text fontsize PIXELS font)))    
    ;;; ved ki a lista 2. elemét
    (set! text-height (elem 2  (gimp-text-get-extents-fontname text fontsize PIXELS font)))    
    ;;;    
    
    (list text-width text-height)
    )
)


;Maga a scritp
(define (script-fu-bhax-mandala text text2 font fontsize width height color gradient)
(let* ;deklaráció
    (
        (image (car (gimp-image-new width height 0)))
        (layer (car (gimp-layer-new image width height RGB-IMAGE "bg" 100 LAYER-MODE-NORMAL-LEGACY)))
        (textfs)
        (text-layer)
        (text-width (text-width text font fontsize))
        ;;;
        (text2-width (car (text-wh text2 font fontsize)))
        (text2-height (elem 2 (text-wh text2 font fontsize)))
        ;;;
        (textfs-width)
        (textfs-height)
        (gradient-layer)
    )
	;beilleszti a réteget a képre
    (gimp-image-insert-layer image layer 0 0)
	;átállítja az előtér színét, átszinezi a réteget a színre
    (gimp-context-set-foreground '(0 255 0))
    (gimp-drawable-fill layer FILL-FOREGROUND)
    (gimp-image-undo-disable image)  
	
    (gimp-context-set-foreground color)
	;létrehoz egy új szöveg réteget, beilleszti, átállítja az offsetet, újraméretezi a régete.
    (set! textfs (car (gimp-text-layer-new image text font fontsize PIXELS)))
    (gimp-image-insert-layer image textfs 0 -1)
    (gimp-layer-set-offsets textfs (- (/ width 2) (/ text-width 2))  (/ height 2))
    (gimp-layer-resize-to-image-size textfs)
	;új szöveg réteget hoz létre, elfordítja a saját középpontja körül, az alatta lévő réteghez fésüli.
    (set! text-layer (car (gimp-layer-new-from-drawable textfs image)))
    (gimp-image-insert-layer image text-layer 0 -1)
    (gimp-item-transform-rotate-simple text-layer ROTATE-180 TRUE 0 0)
    (set! textfs (car(gimp-image-merge-down image text-layer CLIP-TO-BOTTOM-LAYER)))
	;repeat
    (set! text-layer (car (gimp-layer-new-from-drawable textfs image)))
    (gimp-image-insert-layer image text-layer 0 -1)
    (gimp-item-transform-rotate text-layer (/ *pi* 2) TRUE 0 0)
    (set! textfs (car(gimp-image-merge-down image text-layer CLIP-TO-BOTTOM-LAYER)))
	;repeat
    (set! text-layer (car (gimp-layer-new-from-drawable textfs image)))
    (gimp-image-insert-layer image text-layer 0 -1)
    (gimp-item-transform-rotate text-layer (/ *pi* 4) TRUE 0 0)
    (set! textfs (car(gimp-image-merge-down image text-layer CLIP-TO-BOTTOM-LAYER)))
	;repeat    
    (set! text-layer (car (gimp-layer-new-from-drawable textfs image)))
    (gimp-image-insert-layer image text-layer 0 -1)
    (gimp-item-transform-rotate text-layer (/ *pi* 6) TRUE 0 0)
    (set! textfs (car(gimp-image-merge-down image text-layer CLIP-TO-BOTTOM-LAYER)))    
    ;kivágja a kijelölt réteg egy részét a textfs alapján
    (plug-in-autocrop-layer RUN-NONINTERACTIVE image textfs)
    (set! textfs-width (+ (car(gimp-drawable-width textfs)) 100))
    (set! textfs-height (+ (car(gimp-drawable-height textfs)) 100))
    ;átméretezi a textfs méretét az image méretére
	(gimp-layer-resize-to-image-size textfs)
    ;kijelöl egy ellipszist majd útvonallá alakítja
    (gimp-image-select-ellipse image CHANNEL-OP-REPLACE (- (- (/ width 2) (/ textfs-width 2)) 18)
        (- (- (/ height 2) (/ textfs-height 2)) 18) (+ textfs-width 36) (+ textfs-height 36))
	(plug-in-sel2path RUN-NONINTERACTIVE image textfs)
    ;átállítja az ecset méretét 20-ra és rajzol vele a textfs réteg kijelölésén
    (gimp-context-set-brush-size 22)
    (gimp-edit-stroke textfs)
    
    (set! textfs-width (- textfs-width 70))
    (set! textfs-height (- textfs-height 70))
    ;repeat
    (gimp-image-select-ellipse image CHANNEL-OP-REPLACE (- (- (/ width 2) (/ textfs-width 2)) 18) 
        (- (- (/ height 2) (/ textfs-height 2)) 18) (+ textfs-width 36) (+ textfs-height 36))
	(plug-in-sel2path RUN-NONINTERACTIVE image textfs)
        
    (gimp-context-set-brush-size 8)
    (gimp-edit-stroke textfs)
    ;beállít egy gradient réteget    
    (set! gradient-layer (car (gimp-layer-new image width height RGB-IMAGE "gradient" 100 LAYER-MODE-NORMAL-LEGACY)))
    ;beilleszti a gradient réteget, kijelöli a textfs réteget formájában és gradientet rajzol rá.
    (gimp-image-insert-layer image gradient-layer 0 -1)
	(gimp-image-select-item image CHANNEL-OP-REPLACE textfs)
	(gimp-context-set-gradient gradient)
	(gimp-edit-blend gradient-layer BLEND-CUSTOM LAYER-MODE-NORMAL-LEGACY GRADIENT-RADIAL 100 0 
	REPEAT-TRIANGULAR FALSE TRUE 5 .1 TRUE (/ width 2) (/ height 2) (+ (+ (/ width 2) (/ textfs-width 2)) 8) (/ height 2)) ;beállítja a gradientet
	
	(plug-in-sel2path RUN-NONINTERACTIVE image textfs)
	;beilleszti a BHAX szöveget és kiírja text2 magasságát
    (set! textfs (car (gimp-text-layer-new image text2 font fontsize PIXELS)))
    (gimp-image-insert-layer image textfs 0 -1)
    (gimp-message (number->string text2-height))
    (gimp-layer-set-offsets textfs (- (/ width 2) (/ text2-width 2)) (- (/ height 2) (/ text2-height 2)))
		
    ;(gimp-selection-none image)
    ;(gimp-image-flatten image)
    ;kiírja a képet
    (gimp-display-new image)
    (gimp-image-clean-all image)
    )
)
;beállítja a kép létrehozásához használt értékeket
(script-fu-register "script-fu-bhax-mandala"
    "Mandala9"
    "Creates a mandala from a text box."
    "Norbert Bátfai"
    "Copyright 2019, Norbert Bátfai"
    "January 9, 2019"
    ""
    SF-STRING       "Text"      "Bátf41 Haxor"
    SF-STRING       "Text2"     "BHAX"
    SF-FONT         "Font"      "Sans"
    SF-ADJUSTMENT   "Font size" '(100 1 1000 1 10 0 1)
    SF-VALUE        "Width"     "1000"
    SF-VALUE        "Height"    "1000"
    SF-COLOR        "Color"     '(255 0 0)
    SF-GRADIENT     "Gradient"  "Deep Sea"
)
;beállít neki egy fület a menüben
(script-fu-menu-register "script-fu-bhax-mandala" 
    "<Image>/File/Create/BHAX"
)
