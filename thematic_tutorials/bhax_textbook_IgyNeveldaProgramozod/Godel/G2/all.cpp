#include <iostream>
#include <string>
#include <vector>

template<typename T>
struct CustomAlloc{
	using size_type= size_t;
	using value_type= T;
	using pointer= T*;
	using const_pointer= const T*;
	using reference= T&;
	using const_reference= const T&;
	using difference_type= ptrdiff_t;
	
	CustomAlloc() {}
	CustomAlloc(const CustomAlloc&) {}
	~CustomAlloc() {}
	
	pointer allocate(size_type n)
	{
		std::cout<<"Allocating "<<n<<" object of "<<n*sizeof(value_type)<<" bytes "<<std::endl;
		return reinterpret_cast<pointer>(new char[n*sizeof(value_type)]);
	}
	void deallocate(pointer p, size_type n)
	{
		std::cout<<"Deallocating "<<n<<" object of "<<n*sizeof(value_type)<<" bytes "<<std::endl;
		delete[] reinterpret_cast<char *> (p);
	}
};

int main()
{
	std::string s;	
	std::allocator<int> a;

	std::vector<int, CustomAlloc<int>>v;
	v.push_back(42);
	v.push_back(43);
	v.push_back(44);
}
