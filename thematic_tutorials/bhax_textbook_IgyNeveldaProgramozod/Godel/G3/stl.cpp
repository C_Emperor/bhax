#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

void printmap(std::map <std::string, int> &map)
{
        for ( auto & i : map ) {
		std::cout<<i.first<<", "<<i.second<<std::endl;
        }

}

void printvector(std::vector<std::pair<std::string, int>> vec)
{
	for(auto i: vec)
	{
		std::cout<<i.first<<", "<<i.second<<std::endl;
	}
}

std::vector<std::pair<std::string, int>> sort_map ( std::map <std::string, int> &rank )
{
        std::vector<std::pair<std::string, int>> ordered;

        for ( auto & i : rank ) {
                if ( i.second ) {
                        std::pair<std::string, int> p {i.first, i.second};
                        ordered.push_back ( p );
                }
        }

        std::sort (
                std::begin ( ordered ), std::end ( ordered ),
        [ = ] ( auto && p1, auto && p2 ) {
                return p1.second > p2.second;
        }
        );

        return ordered;
}

int main()
{
	std::map <std::string, int> obj; 
	obj["A"]=7;
	obj["X"]=1;
	obj["C"]=11;
	obj["H"]=10;
	printmap(obj);
	std::cout<<"Sorting"<<std::endl;
	std::vector<std::pair<std::string, int>> sorted = sort_map(obj);
	printvector(sorted);
}
