public class PolarGen{

  boolean nincsTárolt=true;
  double tárolt;

  public PolarGen()
  {
    nincsTárolt=true;
  }
  public double következő()
  {
    if(nincsTárolt)
    {
      double u1, u2, v1, v2, w;
      do
      {
        u1=Math.random(); //A random 0 és 1 között generál random számokat
        u2=Math.random(); //A random 0 és 1 között generál random számokat
        v1=2*u1-1; //A random számot megszorozzuk kettővel és kivonunk belőle egyet, így (nagyjából) -1 és 1 közötti érték lesz.
        v2=2*u2-1; //A random számot megszorozzuk kettővel és kivonunk belőle egyet, így (nagyjából) -1 és 1 közötti érték lesz.
        w=v1*v1+v2*v2; //Pitegorasz tétel
      }while(w>1);
      double r=Math.sqrt((-2*Math.log(w))/w);
      tárolt=r*v2;
      nincsTárolt=!nincsTárolt;
      return r*v1;
    }
    else
    {
      nincsTárolt=!nincsTárolt;
      return tárolt;
    }
  }
  public static void main(String[] args)
  {
      PolarGen g = new PolarGen();
      for(int i=0; i<10; ++i)
      {
        System.out.println(g.következő());
      }
  }
}
