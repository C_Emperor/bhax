#ifndef POLARGEN__H
#define POLARGEN__H

#include <cstdlib>
#include <cmath>
#include <ctime>

class PolarGen
{
public:
    PolarGen()
    {
        nincstarolt=true;
        std::srand (std::time (NULL));
    }
    ~PolarGen()
    {
    }
    double kovetkezo();
private:    
    bool nincstarolt;
    double tarolt;
};

#endif
