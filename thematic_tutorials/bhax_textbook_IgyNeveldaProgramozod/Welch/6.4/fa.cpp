#include <iostream>		
#include <fstream>		

class LZWBinFa
{
public:

  LZWBinFa ():fa (&gyoker)
  {
  }
   ~LZWBinFa ()
  {
    szabadit (gyoker.egyesGyermek ());
    szabadit (gyoker.nullasGyermek ());
  }
  void operator<< (char b)
  {
    if (b == '0')
      {
	if (!fa->nullasGyermek ())	
	  {
	    Csomopont *uj = new Csomopont ('0');
	    fa->ujNullasGyermek (uj);
	    fa = &gyoker;
	  }
	else		
	  {
	    fa = fa->nullasGyermek ();
	  }
      }
    else
      {
	if (!fa->egyesGyermek ())
	  {
	    Csomopont *uj = new Csomopont ('1');
	    fa->ujEgyesGyermek (uj);
	    fa = &gyoker;
	  }
	else
	  {
	    fa = fa->egyesGyermek ();
	  }
      }
  }
  void kiir (void)
  {
    melyseg = 0;
    kiir (&gyoker, std::cout);
  }
  friend std::ostream & operator<< (std::ostream & os, LZWBinFa & bf)
  {
    bf.kiir (os);
    return os;
  }
  void kiir (std::ostream & os)
  {
    melyseg = 0;
    kiir (&gyoker, os);
  }

private:
  class Csomopont
  {
  public:
  Csomopont (char b = '/'):betu (b), balNulla (0), jobbEgy (0)
    {
    };
    ~Csomopont ()
    {
    };
    Csomopont *nullasGyermek () const
    {
      return balNulla;
    }
    Csomopont *egyesGyermek () const
    {
      return jobbEgy;
    }
    void ujNullasGyermek (Csomopont * gy)
    {
      balNulla = gy;
    }
    void ujEgyesGyermek (Csomopont * gy)
    {
      jobbEgy = gy;
    }
    char getBetu () const
    {
      return betu;
    }

  private:
    char betu;
    Csomopont *balNulla;
    Csomopont *jobbEgy;
    Csomopont (const Csomopont &); 
    Csomopont & operator= (const Csomopont &);
  };
  Csomopont *fa;
  int melyseg;
  LZWBinFa (const LZWBinFa &);
  LZWBinFa & operator= (const LZWBinFa &);
  void kiir (Csomopont * elem, std::ostream & os)
  {
    if (elem != NULL)
      {
	++melyseg;
	kiir (elem->egyesGyermek (), os);
	for (int i = 0; i < melyseg; ++i)
	  os << "---";
	os << elem->getBetu () << "(" << melyseg - 1 << ")" << std::endl;
	kiir (elem->nullasGyermek (), os);
	--melyseg;
      }
  }
  void szabadit (Csomopont * elem)
  {
    if (elem != NULL)
      {
	szabadit (elem->egyesGyermek ());
	szabadit (elem->nullasGyermek ());
	delete elem;
      }
  }

protected:	
  Csomopont gyoker;
  int maxMelyseg;
};
int
main ()
{
    char b;
    LZWBinFa binFa;
    while (std::cin >> b)
    {
	if(b=='x')
		break;        
	binFa << b;
    }
    std::cout << binFa; 
    return 0;
}
