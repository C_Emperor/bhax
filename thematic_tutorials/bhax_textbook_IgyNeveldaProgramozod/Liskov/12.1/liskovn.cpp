#include <iostream>
// ez a T az LSP-ben
class Auto {
public:
     virtual void auto_park() {};
};

// ez a két osztály alkotja a "P programot" az LPS-ben
class Program {
public:
     void run ( Auto &car ) {
          car.auto_park();
     }
};

// itt jönnek az LSP-s S osztályok
class Tesla : public Auto
{
  void auto_park()
  {
    std::cout<<"It's as easy as that."<<std::endl;
  }
};

class Lada : public Auto
{
  void auto_park()
  {
    std::cout<<"The hell kind of capitalist garbage is this, Vladimir?"<<std::endl;
  }
};

class FordT1 : public Auto
{
  void auto_park()
  {
    std::cout<<"What sort of sorcery is that, John? 'Tis a ghost riding thy vehicle?"<<std::endl;
  }
};

int main ( int argc, char **argv )
{
     Program p;

     Tesla t;
     p.run(t);

     Lada l;
     p.run(l);

     FordT1 f;
     p.run(f);
}
