class Liskovn
{
  static class Auto{
    public void auto_park(){};
  };

  static class Prog{
        public void run(Auto auto)
        {
          auto.auto_park();
        }
    };
  static class Tesla extends Auto
  {
    @Override
    public void auto_park()
    {
      System.out.println("It's as easy as that.");
    }
  }
  static class Lada extends Auto
  {
    @Override
    public void auto_park()
    {
      System.out.println("The hell kind of capitalist garbage is this, Vladimir?");
    }
  }
  static class FordT1 extends Auto
  {
    @Override
    public void auto_park()
    {
      System.out.println("What sort of sorcery is that, John? 'Tis a ghost riding thy vehicle?");
    }
  }
    public static void main(String[] args)
    {
      Prog p = new Prog();

      Tesla t = new Tesla();
      p.run(t);

      Lada l = new Lada();
      p.run(l);

      FordT1 f = new FordT1();
      p.run(f);
    }
}
