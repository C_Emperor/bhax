package ex;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FutureChainingExercise {
	private static ExecutorService executorService = Executors.newFixedThreadPool(2); //ExecutorService létrehozása 2 szállal
	public static void main(String[] args) {
		CompletableFuture<String> longTask = CompletableFuture.supplyAsync(() -> {
			sleep(1000);
			return "Hello";
		}, executorService);
		CompletableFuture<String> shortTask = CompletableFuture.supplyAsync(() -> {
			sleep(500);
			return "Hi";
		}, executorService);
		CompletableFuture<String> mediumTask = CompletableFuture.supplyAsync(() -> {
			sleep(750);
			return "Hey";
		}, executorService);
		CompletableFuture<String> result = longTask.applyToEitherAsync(shortTask, String::toUpperCase, executorService);
		result = result.thenApply(s -> s + " World");
		CompletableFuture<Void> extraLongTask
		= CompletableFuture.supplyAsync(() -> {
			sleep(1500);
			return null;
		}, executorService);
		result = result.thenCombineAsync(mediumTask, (s1, s2) -> s2 + ", " + s1, executorService);
		System.out.println(result.getNow("Bye"));
		sleep(1500);
		System.out.println(result.getNow("Bye"));
		result.runAfterBothAsync(extraLongTask, () -> System.out.println("After both!"), executorService);
		result.whenCompleteAsync((s, throwable) -> System.out.println("Complete: " 	+ s), executorService);
		executorService.shutdown();
	}
	/**
	*
	* @param sleeptime sleep time in milliseconds
	*/
	private static void sleep(int sleeptime) {
		try {
			Thread.sleep(sleeptime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

