import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Dropout, Flatten, MaxPooling2D

from PIL import Image
import numpy as np
import sys

def evaluateImage(input_image):
    pred = model.predict(input_image.reshape(1, 28, 28, 1))
    print(pred)
    print("The number is = ", pred.argmax())

argumentCount = len(sys.argv) - 1
print ("The script is called with %i arguments" % (argumentCount))
if argumentCount > 0:
    tf.compat.v1.enable_eager_execution(
        config=None, device_policy=None, execution_mode=None
    )

    mnist = tf.keras.datasets.mnist

    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    x_train, x_test = x_train / 255.0, x_test / 255.0

    model = tf.keras.models.Sequential([
        tf.keras.layers.Flatten(input_shape=(28, 28)),
        tf.keras.layers.Dense(128, activation='relu'),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Dense(10)
    ])

    predictions = model(x_train[:1]).numpy()
    predictions

    tf.nn.softmax(predictions).numpy()

    loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

    loss_fn(y_train[:1], predictions).numpy()

    model.compile(optimizer='adam',
                  loss=loss_fn,
                  metrics=['accuracy'])

    model.fit(x_train, y_train, epochs=5)

    model.evaluate(x_test,  y_test, verbose=2)

    probability_model = tf.keras.Sequential([
      model,
      tf.keras.layers.Softmax()
    ])

    probability_model(x_test[:5])

    for i in range(1, argumentCount+1):
        input_image = np.array(Image.open(sys.argv[i]).getdata(0).resize((28, 28), 0))
        evaluateImage(input_image)

else:
    print("Program használata: python3 mnist.py <képfile>")

