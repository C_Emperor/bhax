public class Bub
{
	int[] tarolt;
	int size;
	int index = 0;
	public Bub(int n)
	{
		tarolt=new int[n];
		size = n;
	}
	public void print()
	{
		System.out.print("[");
		int n = 0;
		for(int i=0; i<size-1; i++)
		{
			System.out.print(tarolt[i]+",");
			n++;
		}
		System.out.print(tarolt[n]+"]");
		System.out.println(" ");
		System.out.println(" ");
	}
	public void add(int n)
	{
		if(index<size)
		{
			if(!(this.BinSearch(n)))
			{
				tarolt[index]=n;
				index++;
				this.BubSort();
			}
			else
			{
				System.out.println("Ez az elem már a tömbben van!");
			}

		}
		else
		System.out.println("A tömb betelt");
	}
	public boolean BinSearch(int x)
	{
		int[] arr=this.tarolt;
		return BinSearch(arr, 0, arr.length-1, x);
	}
	public boolean BinSearch(int arr[], int l, int r, int x)
	{
	        if (r >= l) { 
            int mid = l + (r - l) / 2; 
 	           if (arr[mid] == x) 
 	               return true; 

 	           if (arr[mid] < x) 
  	              return BinSearch(arr, l, mid - 1, x); 

 	           if (arr[mid] > x) 
	         	   return BinSearch(arr, mid + 1, r, x); 
         	   
         	   
	        } 
	        return false; 
	}
	public void BubSort()
	{
		for(int i=0; i<size; i++)
		{
			for(int j=i; j<size; j++)
			{
				if(tarolt[i]<tarolt[j])
				{
					int tmp=tarolt[i];
					tarolt[i] = tarolt[j];
					tarolt[j] = tmp;
				}
			}		
			
		}		
	}
	public static void main(String[] args)
	{
		Bub t = new Bub(6);
		t.add(1);
		t.add(7);
		t.add(22);
		t.add(3);
		t.add(10);
		t.add(66);
		t.print();
		
	}
}
