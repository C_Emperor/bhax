#include <stdio.h>
int main()
{
	unsigned long long int length = 1; //Max érték 2^63
	int bit = 0;
	do
	{
		bit++; 
	}
	while(length <<= 1); //Meddig képes eltolni a bitet (Szorozni a word számot kettővel)
	printf("A szóhossz ezen a gépen %i bit\n", bit);
	return 0;
}
