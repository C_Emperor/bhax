#include <stdio.h>
#include <math.h>
void kiir(double tomb[], int n)
{
	for(int i=0; i<n; i++)
	{
		printf("%f\n", tomb[i]);
	}
}
double tavolsag(double PR[], double PRv[], int n)
{
	double sum=0;
	for (int i=0; i<n; i++)
		sum +=  pow((PRv[i]-PR[i]),2);
	return sqrt(sum);
	
}

int main()
{
	double L[4][4] = {
	  {0.0, 0.0, 1.0 / 3.0, 0.0},
	  {1.0, 1.0 / 2.0, 1.0 / 3.0, 1.0},
	  {0.0, 1.0 / 2.0, 0.0, 0.0},
	  {0.0, 0.0, 1.0 / 3.0, 0.0}
	};
	double PR[4] = { 0.0, 0.0, 0.0, 0.0 };
	double PRv[4] = { 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0 };
	while(1)
	{
		for(int i=0; i<4; i++){
			PR[i]=0.0;
			for(int j=0; j<4; j++)
				PR[i] += (L[i][j] * PRv[j]);
		}
		if (tavolsag(PR, PRv, 4) < 0.000000001)
			break;
		for(int i=0; i<4; i++)
			PRv[i] = PR[i];
	}
	kiir(PR, 4);
	return 0;
}
