
#ifndef GUN_H
#define GUN_H


/**
  * class Gun
  * 
  */

class Gun
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Gun();

  /**
   * Empty Destructor
   */
  virtual ~Gun();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   */
  void Shoot()
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  

  int Mag_Size;

  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //  


  /**
   * Set the value of Mag_Size
   * @param new_var the new value of Mag_Size
   */
  void setMag_Size(int new_var)
  {
    Mag_Size = new_var;
  }

  /**
   * Get the value of Mag_Size
   * @return the value of Mag_Size
   */
  int getMag_Size()
  {
    return Mag_Size;
  }

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //  


  void initAttributes();

};

#endif // GUN_H
