
#ifndef STAFF_H
#define STAFF_H


/**
  * class Staff
  * 
  */

class Staff
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Staff();

  /**
   * Empty Destructor
   */
  virtual ~Staff();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   */
  void cast()
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  

  int mana_pool;

  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //  


  /**
   * Set the value of mana_pool
   * @param new_var the new value of mana_pool
   */
  void setMana_pool(int new_var)
  {
    mana_pool = new_var;
  }

  /**
   * Get the value of mana_pool
   * @return the value of mana_pool
   */
  int getMana_pool()
  {
    return mana_pool;
  }

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //  


  void initAttributes();

};

#endif // STAFF_H
