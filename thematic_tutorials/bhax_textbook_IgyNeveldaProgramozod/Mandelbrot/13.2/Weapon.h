
#ifndef WEAPON_H
#define WEAPON_H

#include "Sword.h"
#include "Gun.h"
#include "Staff.h"


/**
  * class Weapon
  * 
  */

class Weapon : public Sword, public Gun, public Staff
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Weapon();

  /**
   * Empty Destructor
   */
  virtual ~Weapon();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  

  int Dammage;
  int Weight;
  int item_id;
  int value;

  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //  


  /**
   * Set the value of Dammage
   * @param new_var the new value of Dammage
   */
  void setDammage(int new_var)
  {
    Dammage = new_var;
  }

  /**
   * Get the value of Dammage
   * @return the value of Dammage
   */
  int getDammage()
  {
    return Dammage;
  }

  /**
   * Set the value of Weight
   * @param new_var the new value of Weight
   */
  void setWeight(int new_var)
  {
    Weight = new_var;
  }

  /**
   * Get the value of Weight
   * @return the value of Weight
   */
  int getWeight()
  {
    return Weight;
  }

  /**
   * Set the value of item_id
   * @param new_var the new value of item_id
   */
  void setItem_id(int new_var)
  {
    item_id = new_var;
  }

  /**
   * Get the value of item_id
   * @return the value of item_id
   */
  int getItem_id()
  {
    return item_id;
  }

  /**
   * Set the value of value
   * @param new_var the new value of value
   */
  void setValue(int new_var)
  {
    value = new_var;
  }

  /**
   * Get the value of value
   * @return the value of value
   */
  int getValue()
  {
    return value;
  }

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //  


  void initAttributes();

};

#endif // WEAPON_H
