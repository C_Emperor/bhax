
#ifndef SWORD_H
#define SWORD_H


/**
  * class Sword
  * 
  */

class Sword
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Sword();

  /**
   * Empty Destructor
   */
  virtual ~Sword();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   */
  void Swing()
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  

  int Durability;

  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //  


  /**
   * Set the value of Durability
   * @param new_var the new value of Durability
   */
  void setDurability(int new_var)
  {
    Durability = new_var;
  }

  /**
   * Get the value of Durability
   * @return the value of Durability
   */
  int getDurability()
  {
    return Durability;
  }

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //  


  void initAttributes();

};

#endif // SWORD_H
