﻿#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define MAX_KULCS 100			//Legfeljebb mekkora lehet a kulcs
#define BUFFER_MERET 256		//Legfeljebb mekkora lehet a titkosítandó/dekódolandó szöveg.


int
main (int argc, char **argv)
{

  char kulcs[MAX_KULCS];		//A kulcs amivel titkosítjuk a szöveget
  char buffer[BUFFER_MERET];		//A szöveg amit titkosítunk/dekódolunk

  int kulcs_index = 0;			//
  int olvasott_bajtok = 0;		//

  int kulcs_meret = strlen (argv[1]);	//A kulcs hossza
  strncpy (kulcs, argv[1], MAX_KULCS);	//A szöveg átmásosála a bemenetből a változóba

  while ((olvasott_bajtok = read (0, (void *) buffer, BUFFER_MERET)))
    {

      for (int i = 0; i < olvasott_bajtok; ++i)
	{

	  buffer[i] = buffer[i] ^ kulcs[kulcs_index]; //A szöveg i-edik karakterén XOR műveletet hajtunk végre a kulcs i-edik elemével
	  kulcs_index = (kulcs_index + 1) % kulcs_meret; //Kulcs indexének növelése, amint eléri a kulcs méretét visszalép az egyes indexre

	}

      write (1, buffer, olvasott_bajtok);

    }
}
